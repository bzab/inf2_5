import math
from collections import deque

def floor(x):
    return int(math.floor(x))

def sgn(x):
    return (0 < x) - (x < 0)

def enum(**enums):
    return type('Enum', (), enums)

class IndexedDeque(deque):
    def index(self, element):
        for i, el in enumerate(self):
            if el == element:
                return i
        raise ValueError("{} is not in queue".format(element))


class IntVector2(object):
    def __init__(self, x=0, y=0, vec=None):
        if (not (isinstance(x, int) and isinstance(y, int)) and
           not isinstance(vec, IntVector2)):
            raise ValueError(
               "Required are 2 ints or IntVector2")
        else:
            self.x = x
            self.y = y
        
    def __repr__(self):
        return "IntVector2({x}, {y})".format(x=self.x, y=self.y)
    
    def __str__(self):
        return "({x}, {y})".format(x=self.x, y=self.y)
    
    def __hash__(self):
        return hash(self.x, self.y)

    def __getitem__(self, key):
        if key < 0:
            key += 2
        if key == 0:
            return self.x
        elif key == 1:
            return self.y
        else:
            raise IndexError
    
    def __setitem__(self, key, val):
        if key < 0:
            key += 2
        if key == 0:
            self.x = val
        elif key == 1:
            self.y = val
        else:
            raise IndexError
    
    def __eq__(self, op):
        if isinstance(op, IntVector2):
            return (self.x==op.x) and (self.y==op.y)
        elif (
           isinstance(op, tuple) and len(op)==2 and 
           all([isinstance(el, int) for el in op]) ):
            return (self.x==op[0]) and (self.y==op[1])
            
    def __ne__(self, op):
        if isinstance(op, IntVector2):
            return (self.x!=op.x) or (self.y!=op.y)
        elif (
           isinstance(op, tuple) and len(op)==2 and 
           all([isinstance(el, int) for el in op]) ):
            return (self.x!=op[0]) or (self.y!=op[1])
    
    def __add__(self, op):
        if isinstance(op, IntVector2):
            x = self.x + op.x
            y = self.y + op.y
            return IntVector2(x, y)
        elif (
           isinstance(op, tuple) and len(op)==2 and 
           all([isinstance(el, int) for el in op]) ):
            x = self.x + op[0]
            y = self.y + op[1]
            return IntVector2(x,y)
        else:
            raise ValueError(
               "Required is an instance of class IntVector2 or tuple of 2 ints")

    def __radd__(self, op):
        return self.__add__(op)
        
    def __iadd__(self, op):
        if isinstance(op, IntVector2):
            self.x += op.x
            self.y += op.y
            return self
        elif (
           isinstance(op, tuple) and len(op)==2 and 
           all([isinstance(el, int) for el in op]) ):
            self.x += op[0]
            self.y += op[1]
            return self
        else:
            raise ValueError(
               "Required is an instance of class IntVector2 or tuple of 2 ints")
               
    def __sub__(self, op):
        if isinstance(op, tuple):
            return self.__add__(tuple([-1*el for el in op]))
        else:
            return self.__add__(-1*op)

    def __rsub__(self, op):
        if isinstance(op, tuple):
            return -1 * self.__add__(tuple([-1*el for el in op]))
        else:
            return -1 * (self.__add__(-1*op))
        
    def __isub__(self, op):
        if isinstance(op, tuple):
            return self.__iadd__(tuple([-1*el for el in op]))
        else:
            return self.__iadd__(-1*op)
    
    def __mul__(self, op):
        if isinstance(op, int) or isinstance(op, float):
            x = self.x * op
            y = self.y * op
            if isinstance(op, float):
                x = floor(x)
                y = floor(y)
            return IntVector2(x, y)
        else:
            raise ValueError(
               "Required is a number as an operand")
    
    def __rmul__(self,op):
        return self.__mul__(op)
        
    def __imul__(self, op):
        if isinstance(op, int) or isinstance(op, float):
            self.x *= op
            self.y *= op
            if isinstance(op, float):
                self.x = floor(self.x)
                self.y = floor(self.y)
            return self
        else:
            raise ValueError(
               "Required is a number as an operand")
    
    def __div__(self,op):
        return self.__mul__(1.0/op)
        
    def __idiv__(self,op):
        return self.__imul__(1.0/op)


class IntVector3(object):
    def __init__(self, x=0, y=0, z=0, vec=None):
        if (not (isinstance(x, int) and isinstance(y, int) and 
           isinstance(z, int)) and not isinstance(vec, IntVector3)):
            raise ValueError(
               "Required are 3 ints or IntVector3")
        else:
            self.x = x
            self.y = y
            self.z = z
    
    def __repr__(self):
        return "IntVector3({x}, {y}, {z})".format(x=self.x, y=self.y, z=self.z)
    
    def __str__(self):
        return "({x}, {y}, {z})".format(x=self.x, y=self.y, z=self.z)
    
    def __hash__(self):
        return hash(self.x, self.y, self.z)

    def __getitem__(self, key):
        if key < 0:
            key += 3
        if key == 0:
            return self.x
        elif key == 1:
            return self.y
        elif key == 2:
            return self.z
        else:
            raise IndexError
    
    def __setitem__(self, key, val):
        if key < 0:
            key += 3
        if key == 0:
            self.x = val
        elif key == 1:
            self.y = val
        elif key == 2:
            self.z = val
        else:
            raise IndexError
    
    def __eq__(self, op):
        if isinstance(op, IntVector3):
            return (self.x==op.x) and (self.y==op.y) and (self.z==op.z)
        elif (
           isinstance(op, tuple) and len(op)==3 and 
           all([isinstance(el, int) for el in op]) ):
            return (self.x==op[0]) and (self.y==op[1]) and (self.z==op[2])
            
    def __ne__(self, op):
        if isinstance(op, IntVector3):
            return (self.x!=op.x) or (self.y!=op.y) and (self.z!=op.z)
        elif (
           isinstance(op, tuple) and len(op)==3 and 
           all([isinstance(el, int) for el in op]) ):
            return (self.x!=op[0]) or (self.y!=op[1]) and (self.z==op[2])
    
    def __add__(self, op):
        if isinstance(op, IntVector3):
            x = self.x + op.x
            y = self.y + op.y
            z = self.z + op.z
            return IntVector3(x, y, z)
        elif (
           isinstance(op, tuple) and len(op)==3 and 
           all([isinstance(el, int) for el in op]) ):
            x = self.x + op[0]
            y = self.y + op[1]
            z = self.z + op[2]
            return IntVector3(x,y,z)
        else:
            raise ValueError(
              "Required is an instance of class IntVector3 or tuple of 3 ints")

    def __radd__(self, op):
        return self.__add__(op)
        
    def __iadd__(self, op):
        if isinstance(op, IntVector3):
            self.x += op.x
            self.y += op.y
            self.z += op.z
            return self
        elif (
           isinstance(op, tuple) and len(op)==3 and 
           all([isinstance(el, int) for el in op]) ):
            self.x += op[0]
            self.y += op[1]
            self.z += op[2]
            return self
        else:
            raise ValueError(
              "Required is an instance of class IntVector3 or tuple of 3 ints")
               
    def __sub__(self, op):
        if isinstance(op, tuple):
            return self.__add__(tuple([-1*el for el in op]))
        else:
            return self.__add__(-1*op)

    def __rsub__(self, op):
        if isinstance(op, tuple):
            return -1 * self.__add__(tuple([-1*el for el in op]))
        else:
            return -1 * (self.__add__(-1*op))
        
    def __isub__(self, op):
        if isinstance(op, tuple):
            return self.__iadd__(tuple([-1*el for el in op]))
        else:
            return self.__iadd__(-1*op)
    
    def __mul__(self, op):
        if isinstance(op, int) or isinstance(op, float):
            x = self.x * op
            y = self.y * op
            z = self.z * op	
            if isinstance(op, float):
                x = floor(x)
                y = floor(y)
                z = floor(z)
            return IntVector3(x, y, z)
        else:
            raise ValueError(
               "Required is a number as an operand")
    
    def __rmul__(self,op):
        return self.__mul__(op)
        
    def __imul__(self, op):
        if isinstance(op, int) or isinstance(op, float):
            self.x *= op
            self.y *= op
            self.z *= op
            if isinstance(op, float):
                self.x = floor(self.x)
                self.y = floor(self.y)
                self.z = floor(self.z)
            return self
        else:
            raise ValueError(
               "Required is a number as an operand")
    
    def __div__(self,op):
        return self.__mul__(1.0/op)
        
    def __idiv__(self,op):
        return self.__imul__(1.0/op) 
