import pyglet

def set_marker_anchors(marker, square):
    marker.anchor_x = -((square.width - marker.width) // 2)
    marker.anchor_y = -((square.height - marker.height) // 2)
    
pyglet.resource.path = ["./res","./res/img"]
pyglet.resource.reindex()

pyglet.resource.add_font('Visitor TT1 -BRK-.ttf')

images = {
'active_square'    : "active.png",
'box_bl_tr_corner' : "box/bl_tr.png",
'box_br_corner'    : "box/br.png",
'box_tl_corner'    : "box/tl.png",
'box_border'       : "box/border.png",
'box_fill'         : "box/fill.png",
'square'           : "square.png",
'turn_indicator'   : "turn_indicator.png",
'o_marker'         : "o.png",
'unknown_marker'   : "unknown.png",
'x_marker'         : "x.png",
}

for img in images:
    addr_str = images[img]
    images[img] = pyglet.resource.image(addr_str)

for marker in [img for img in images if '_marker' in img]:
    set_marker_anchors(images[marker], images['square'])
