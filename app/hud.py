import pyglet
from app import entities, resources

class AdjustableBox(object):
    def __init__(self, x, y, size_x, size_y, batch=None, group=None):
        self.width = size_x
        self.height = size_y
        
        # Corners
        self.top_left_corner = entities.Sprite(
          img=resources.images['box_tl_corner'], x=x, y=y+size_y-6, 
          batch=batch, group=group)
        
        self.top_right_corner = entities.Sprite(
          img=resources.images['box_bl_tr_corner'], x=x+size_x-6, y=y+size_y, 
          batch=batch, group=group)
        self.top_right_corner.rotation = 90 
        
        self.bottom_left_corner = entities.Sprite(
          img=resources.images['box_bl_tr_corner'], x=x, y=y+2, 
          batch=batch, group=group)
        
        self.bottom_right_corner = entities.Sprite(
          img=resources.images['box_br_corner'], x=x+size_x-6, y=y, 
          batch=batch, group=group)
        
        # Borders
        self.border_left = entities.Sprite(
          img=resources.images['box_border'], x=x, y=y+6, 
          batch=batch, group=group)
        self.border_left.scale_y = size_y - 12
        
        self.border_right = entities.Sprite(
          img=resources.images['box_border'], x=x+size_x-6, y=y+6,
          batch=batch, group=group)
        self.border_right.scale_y = size_y - 8
        
        self.border_top = entities.Sprite(
          img=resources.images['box_border'], x=x+6, y=y+size_y, 
          batch=batch, group=group)
        self.border_top.update(rotation = 90, scale_y = size_x - 12)
        
        self.border_bottom = entities.Sprite(
          img=resources.images['box_border'], x=x+2, y=y+6,
          batch=batch, group=group)
        self.border_bottom.update(rotation = 90, scale_y = size_x - 8)
        
        # Fill
        self.fill = entities.Sprite(img=resources.images['box_fill'], 
          x=x+6, y=y+6, batch=batch, group=group)
        self.fill.update(scale_x = size_x - 12, scale_y = size_y - 12)
        
    def set_position(self, x, y):
        dx = x - self.x
        dy = y - self.y
        
        self.x += dx
        self.y += dy
        
        for sprite in [self.top_left_corner, self.top_right_corner, 
          self.bottom_left_corner, self.bottom_right_corner, self.border_left,
          self.border_right, self.border_top, self.border_bottom, self.fill]:
            sprite.x += dx
            sprite.y += dy
               

class GameLabel(object):
    def __init__(self, font_size, x, y, group_fg=None, group_bg=None, *args, 
      **kwargs):
        if 'group' in kwargs:
            g = kwargs.pop('group')
            if not ('group_fg' in kwargs or 'group_bg' in kwargs):
                group_fg = g
        if group_fg:
            self.fg = pyglet.text.Label(font_name='Visitor TT1 (BRK)', 
              color=(196,196,196,255), font_size=font_size, x=x, y=y, 
              group=group_fg, *args, **kwargs)
        else:
            self.fg = pyglet.text.Label(font_name='Visitor TT1 (BRK)', 
              color=(196,196,196,255), font_size=font_size, x=x, y=y, 
              *args, **kwargs)
        if group_bg:
            self.bg = pyglet.text.Label(font_name='Visitor TT1 (BRK)', 
              color=(0,0,0,255), font_size=font_size, x=x+2, y=y-2, 
              group=group_bg, *args, **kwargs)
        else:
            self.bg = pyglet.text.Label(font_name='Visitor TT1 (BRK)', 
              color=(0,0,0,255), font_size=font_size, x=x+2, y=y-2, 
              *args, **kwargs)
    
    def __del__(self):
        self.fg.delete()
        self.bg.delete()
                
    def draw(self):
        self.bg.draw()
        self.fg.draw()
    
    def get_height(self):
        return self.fg.content_height+2
    
    def get_text(self):
        return self.fg.text
        
    def get_width(self):
        return self.fg.content_width + 2
        
    def move(self, dx, dy):
        self.fg.x += dx
        self.fg.y += dy
        self.bg.x += dx
        self.bg.y += dy
    
    def set_text(self, text):
        self.fg.text = text
        self.bg.text = text
        
    def set_position(self, x, y):
        self.fg.x = x
        self.fg.y = y
        self.bg.x = x+2
        self.bg.y = y-2


class TextBox(AdjustableBox):
    def __init__(self, text="", x=0, y=0, padding_x=10, padding_y=10,
          batch=None, group_bg=None, group_mg=None, group_fg=None,
          center=False):
        
        FONT_SIZE = 30
        CHARACTER_HEIGHT = FONT_SIZE * 2 // 3
        CHARACTER_SPACE_LEN = FONT_SIZE * 2 // 15
        CHARACTER_WIDTH = FONT_SIZE * 2 // 3 + CHARACTER_SPACE_LEN
        LINE_HEIGHT = CHARACTER_HEIGHT + 2
        L_BORDER_WIDTH = 6
        R_BORDER_WIDTH = 6
        T_BORDER_HEIGHT = 6
        B_BORDER_HEIGHT = 6
        BORDERS_WIDTH = L_BORDER_WIDTH + R_BORDER_WIDTH
        BORDERS_HEIGHT = T_BORDER_HEIGHT + B_BORDER_HEIGHT
          
        text_lines = [line.strip() for line in text.split('\n')]
                   
        # Create labels
        self.text = [GameLabel(
            font_size=FONT_SIZE,
            x = x + L_BORDER_WIDTH + padding_x,
            y = (y + B_BORDER_HEIGHT + (len(text_lines)-n-1)*(padding_y 
              + LINE_HEIGHT) + padding_y),
            group_fg=group_fg,
            group_bg=group_mg,
            batch=batch,
            text=line,
          ) for n, line in enumerate(text_lines)]
         
        # Calculate box size
        longest_line = max(line.get_width() for line in self.text)      
        size_x = (BORDERS_WIDTH + 2*padding_x + longest_line)
        size_y = (BORDERS_HEIGHT + len(text_lines) * (LINE_HEIGHT+padding_y) 
          + padding_y)
        super(TextBox, self).__init__(x=x, y=y, size_x=size_x, 
          size_y=size_y, batch=batch, group=group_bg)
        
        if center:
            for line in self.text:
                line.move((longest_line-line.get_width())//2 , 0)
          
        self.x = x
        self.y = y
    
    def move(self, dx, dy):
        super(TextBox, self).set_position(self.x+dx, self.y+dy)
        
        for label in self.text:
            label.move(dx, dy)        
    
    def set_position(self, x, y):
        dx = x - self.x
        dy = y - self.y
        
        super(TextBox, self).set_position(x,y)
        
        for label in self.text:
            label.move(dx, dy)


class PlayersBox(AdjustableBox):
    def __init__(self, players, x=0, y=0, padding_x=10, padding_y=10,
          batch=None, group_bg=None, group_mg=None, group_fg=None):
        
        FONT_SIZE = 30
        CHARACTER_HEIGHT = FONT_SIZE * 2 // 3
        CHARACTER_SPACE_LEN = FONT_SIZE * 2 // 15
        CHARACTER_WIDTH = FONT_SIZE * 2 // 3 + CHARACTER_SPACE_LEN
        L_BORDER_WIDTH = 6
        R_BORDER_WIDTH = 6
        T_BORDER_HEIGHT = 6
        B_BORDER_HEIGHT = 6
        BORDERS_WIDTH = L_BORDER_WIDTH + R_BORDER_WIDTH
        BORDERS_HEIGHT = T_BORDER_HEIGHT + B_BORDER_HEIGHT
        
        # Create sprites and check their size
        self.turn_indicator = entities.Sprite(
          img=resources.images['turn_indicator'], batch=batch, group=group_fg)
        self.markers = [
          entities.Marker(player.marker_type,
            batch=batch, group=group_fg) for player in players ]
        
        widest_marker = max(marker.width for marker in self.markers)
        highest_sprite = max(max(marker.width for marker in self.markers),
          self.turn_indicator.height)
               
        self.names = [
          GameLabel(
            font_size=FONT_SIZE,
            x=(x+L_BORDER_WIDTH+3*padding_x+self.turn_indicator.width
              +widest_marker),
            y=(y+B_BORDER_HEIGHT+padding_y+(len(players)-n-1)
              *(highest_sprite+padding_y)+(highest_sprite-CHARACTER_HEIGHT+2)
              //2), # CHARACTER_HEIGT + 2 -> additional 2px for shadow
            group_fg=group_fg,
            group_bg=group_mg,
            batch=batch,
            text=player.name,
          ) for n, player in enumerate(players)]
        
        # Calculate box size
        longest_name = max(name.get_width() for name in self.names)
        size_x = (BORDERS_WIDTH + 4*padding_x + self.turn_indicator.width
          + widest_marker + longest_name)
        size_y = BORDERS_HEIGHT + 2*highest_sprite + 3*padding_y
        super(PlayersBox, self).__init__(x=x, y=y, size_x=size_x, 
          size_y=size_y, batch=batch, group=group_bg)
        
        # Update position of sprites
        new_x = x+L_BORDER_WIDTH+padding_x+self.turn_indicator.image.anchor_x
        new_y = (y+B_BORDER_HEIGHT+2*padding_y+highest_sprite-(highest_sprite
          -self.turn_indicator.height)//2+self.turn_indicator.image.anchor_y)
        self.turn_indicator.update(x=new_x, y=new_y)
        for num, m in enumerate(self.markers):
            n = len(players) - num - 1
            new_x = (x+L_BORDER_WIDTH+2*padding_x+self.turn_indicator.width
              +m.image.anchor_x)
            new_y = (y+B_BORDER_HEIGHT+(1+n)*padding_y+n*highest_sprite
              -(highest_sprite-m.height)//2+m.image.anchor_y)
            m.update(x=new_x, y=new_y)
          
        self.line_height = highest_sprite
        self.padding_y = padding_y
        self.players_num = len(players)
        self.x = x
        self.y = y
        self.x_max = x + size_x - R_BORDER_WIDTH
        self.x_min = x + L_BORDER_WIDTH
        self.y_max = y + size_y - T_BORDER_HEIGHT
        self.y_min = y + B_BORDER_HEIGHT
    
    def set_position(self, x, y):
        dx = x - self.x
        dy = y - self.y
        
        super(PlayersBox, self).set_position(x,y)
        
        self.x_max += dx
        self.x_min += dx
        self.y_max += dy
        self.y_min += dy
        
        for label in self.names:
            label.move(dx, dy)
        
        self.turn_indicator.x += dx
        self.turn_indicator.y += dy
        for marker in self.markers:
            marker.x += dx
            marker.y += dy        
        
    def set_indicator(self, player):
        line = next(self.markers.index(marker) for marker in self.markers
          if marker.type == player)
        
        new_y = (self.y_min+(self.players_num-line)
          *self.padding_y+(self.players_num-line-1)*self.line_height
          -(self.line_height-self.turn_indicator.height)//2
          +self.turn_indicator.image.anchor_y)
        self.turn_indicator.y = new_y
