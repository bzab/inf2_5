import copy
import pyglet
import random

from app import entities, game_logger, mcts, resources, utils

CAMERA_SPEED = 5
INIT_SIZE = 15
LIMIT_DIST = 0
#LIMIT_DIST = 10
MAX_ZOOM = 4

class BoardState(object):

    def __init__(self, player, first_marker=None):
        self.fields = utils.IndexedDeque([(utils.IndexedDeque([
          self._spawn_field(m,n) for n in range(0, INIT_SIZE) ])
          ) for m in range(0, INIT_SIZE)] )

        self.added = []
        self.current_player = player
        self.first_marker = first_marker
        self.last_action = None
        self.is_terminal = False
        self.winner = None
        
        
    def _add_column_left(self):
        self.added.append("CL")
        self.fields.appendleft(
           utils.IndexedDeque([ self._spawn_field(0, n) for n in range(
             0, len(self.fields[0])) ]) )
        self.first_marker += (1,0)
        self.last_action.x += 1
        
    def _add_column_right(self):
        self.added.append("CR")
        self.fields.append(
           utils.IndexedDeque([ self._spawn_field(len(self.fields), n) for n
             in range(0, len(self.fields[0])) ]) )
        
    def _add_row_bottom(self):
        self.added.append("RB")
        for n, column in enumerate(self.fields):
            column.appendleft(self._spawn_field(n, 0))
        self.first_marker += (0,1)
        self.last_action.y += 1
        
    def _add_row_top(self):
        self.added.append("RT")
        for n, column in enumerate(self.fields):
            column.append(self._spawn_field(n, len(column)))
        
    def _spawn_field(self, i, j):
        return entities.Field()
        
    def check_append(self, i, j):
        size_i = len(self.fields)
        size_j = len(self.fields[0])
        if i < LIMIT_DIST:
            for _ in range(0, LIMIT_DIST-i):
                self._add_column_left()
        if i > size_i - LIMIT_DIST - 1:
            for _ in range(0, LIMIT_DIST-size_i+i+1):
                self._add_column_right()
        if j < LIMIT_DIST:
            for _ in range(0, LIMIT_DIST-j):
                self._add_row_bottom()
        if j > size_j - LIMIT_DIST - 1:
            for _ in range(0, LIMIT_DIST-size_j+j+1):
                self._add_row_top()
    
    def check_win(self, i, j):
        marker_type = self.fields[i][j].marker
        cnt_max = 0
        winning_dir = (0,0)
        arg_i = i
        arg_j = j
        for direction in [(1,0), (1,1), (0,1), (-1,1)]:
            cnt = 0
            i = arg_i
            j = arg_j
            # Find first same marker in a row
            while True:
                if (i >= 0 and j >= 0 and i < len(self.fields) and
                    j < len(self.fields[0]) and 
                      self.fields[i][j].marker == marker_type):
                    i -= direction[0]
                    j -= direction[1]
                else:
                    break
            # Check length of row
            while True:
                i += direction[0]
                j += direction[1]
                if (i < len(self.fields) and j < len(self.fields[i]) and 
                    self.fields[i][j].marker == marker_type):
                    cnt += 1
                    if cnt > cnt_max:
                        cnt_max = cnt
                        winning_dir = direction
                else:
                    break
        if cnt_max >= 5:
            self.winner = marker_type
            return True
        return False  
        
    def get_actions(self):
        actions = []
        for i in range(len(self.fields)):
            for j in range(len(self.fields[i])):
                if self.fields[i][j].is_empty():
                    actions.append(
                       entities.Action(player=self.current_player, x=i, y=j))
        return actions
    
    def do_action(self, action):
        new_state = copy.deepcopy(self)
        new_state.added = []
        if not new_state.first_marker:
            new_state.first_marker = utils.IntVector2(action.x, action.y)
        new_state.last_action = action
        new_state.fields[action.x][action.y].marker = action.player
        if new_state.check_win(action.x, action.y):
            new_state.is_terminal = True
        else:
            new_state.check_append(action.x, action.y)
            # Player is either '1' or '2'
            new_state.current_player = (self.current_player % 2) + 1
        return new_state
    
    def get_reward(self):
        # AI plays as player 2 always
        if self.winner == 2:
            return 1
        else:
            return -1
    
class BoardRenderer(object):
    
    def __init__(self, app):
        self.app = app
        self.batch = pyglet.graphics.Batch()
        self.pgOG_squares = pyglet.graphics.OrderedGroup(0)
        self.pgOG_markers = pyglet.graphics.OrderedGroup(1)
        
        self.squares = utils.IndexedDeque([(utils.IndexedDeque([
          self._spawn_square(m,n) for n in range(0, INIT_SIZE) ])
          ) for m in range(0, INIT_SIZE)] )
        self.square_width = self.squares[0][0].width
        self.square_height = self.squares[0][0].height
    
    def _add_column_left(self):
        # Update existing squares
        for column in self.squares:
            for square in column:
                square.x += square.width
                if square.marker:
                    square.marker.x += square.width               
        # Add new row
        self.squares.appendleft(
           utils.IndexedDeque([ self._spawn_square(0, n) for n in range(
             0, len(self.squares[0])) ]) )
             
        # Update camera pos
        self.app.camera.move((self.square_width * self.app.camera.zoom, 0))
        
    def _add_column_right(self):
        self.squares.append(
           utils.IndexedDeque([ self._spawn_square(len(self.squares), n) for n
             in range(0, len(self.squares[0])) ]) )
        
    def _add_row_bottom(self):
        for n, column in enumerate(self.squares):
            # Update existing squares
            for square in column:
                square.y += square.height
                if square.marker:
                    square.marker.y += square.height                
            # Add new square
            column.appendleft(self._spawn_square(n, 0))

        # Update camera pos
        self.app.camera.move((0, self.square_height * self.app.camera.zoom))
        
    def _add_row_top(self):
        for n, column in enumerate(self.squares):
            column.append(self._spawn_square(n, len(column)))
        
    def _spawn_square(self, i, j):
        return entities.Square(batch = self.batch, i = i,
            j = j, group = self.pgOG_squares, marker_group = self.pgOG_markers)
    
    def draw(self):
        self.batch.draw()
    
    def update(self, state):
        for a in state.added:
            if a == "CL":
                self._add_column_left()
            elif a == "CR":
                self._add_column_right()
            elif a == "RB":
                self._add_row_bottom()
            elif a == "RT":
                self._add_row_top()
            else:
                raise ValueError("Expected row/column code; received: "+str(a))
        
        self.squares[state.last_action.x][state.last_action.y].set_marker(
                                                      state.last_action.player)
            
class Board(object):
    
    def __init__(self, app, init_size):
        self.app = app
        self.board_renderer = BoardRenderer(app)
        self.board_state = BoardState(player=random.randint(1,2))
        self.controller = BoardController(self)
        self.game_logger = game_logger.GameLogger(
          fname="testlog", players=self.app.players, indent=0)
        self.mcts = mcts.Mcts(iterationLimit=1000)
        
        self.pgOG_squares = pyglet.graphics.OrderedGroup(0)
        self.pgOG_markers = pyglet.graphics.OrderedGroup(1)
        
        self.is_ongoing = True
        self.run_ai_turn = self.board_state.current_player == 2
 
    def draw(self):
        self.board_renderer.draw()
        self.controller.draw() 
    
    def end_turn(self):
        self.app.end_turn()
    
    def put_marker(self, i, j):
        if not self.is_ongoing:
            return False
        checked_square = self.board_renderer.squares[i][j]
        if checked_square.marker:
            return False
            
        # Else put players marker
        action = entities.Action(player=self.board_state.current_player, x=i, y=j) 
        print action     
        new_state = self.board_state.do_action(action)
        self.update_state(new_state)
        self.run_ai_turn = True
        return True
    
    def reset(self):
        self.board_renderer = BoardRenderer(self.app)
        self.board_state = BoardState(player=random.randint(1,2))
        self.game_logger.clear()

        self.controller.active_square = None
        self.is_ongoing = True
        self.run_ai_turn = False
    
    def update(self, dt):
        # If game ended - wait for space
        if (not self.is_ongoing and
          self.app.key_handler[pyglet.window.key.SPACE]):
            self.app.reset()
        
        if self.run_ai_turn:
            self.run_ai_turn = False
                    # is AIs turn?
            if self.board_state.first_marker:
                AI_action = self.mcts.search(initialState=self.board_state)
            else:
                p = INIT_SIZE/2+(INIT_SIZE%2!=0) # Middle square coords
                AI_action = entities.Action(player=self.board_state.current_player, x=p, y=p)
            print AI_action
            new_state = self.board_state.do_action(AI_action)
            self.update_state(new_state)
    
    def update_state(self, state):
        self.board_state = state
        x = state.last_action.x - state.first_marker.x
        y = state.last_action.y - state.first_marker.y
        self.game_logger.put_marker(state.current_player, x, y)
        self.board_renderer.update(self.board_state)
        self.controller.set_active_square()  
        
        if state.is_terminal:
            self.win(state.winner)  
        
        self.end_turn()
        
    def win(self, marker_type):
        self.is_ongoing = False
        
        self.game_logger.win(marker_type)
        self.game_logger.gen_file()
        
        txt = "{} WINS! \n Press space to restart".format((player.name for player 
          in self.app.players if player.marker_type == marker_type).next())
        self.app.hud.create_popup(txt)
        
class BoardController(object):
    def __init__(self, board):
        self.board = board
        
        self.active_square = None
        self.active_sprite = pyglet.sprite.Sprite(
            img=resources.images['active_square'], x=0, y=0)
        
        board.app.win.push_handlers(self) 

    def draw(self):
       if self.active_square:
           self.active_sprite.draw()

    def on_mouse_press(self, x, y, button, modifiers):
        if button == pyglet.window.mouse.LEFT:
            renderer = self.board.board_renderer
            i = (x + self.board.app.camera.pos.x) // (renderer.square_width * 
              self.board.app.camera.zoom)
            j = (y + self.board.app.camera.pos.y) // (renderer.square_height *
              self.board.app.camera.zoom)
            if (i>=0 and j>=0 and i<len(renderer.squares) and
               j<len(renderer.squares[0]) ):
                checked_square = renderer.squares[i][j]
                if self.active_square == checked_square:
                    if self.board.board_state.current_player == 1: #if players turn
                        self.board.put_marker(i, j)
                    else:
                        self.reset_active_square()
                else:
                    self.set_active_square(checked_square)
                return pyglet.event.EVENT_HANDLED  

    def set_active_square(self, square=None):
        if square:
            self.active_square = square
        if self.active_square:
            self.active_sprite.update(x=self.active_square.x,
                                      y=self.active_square.y)
    
    def reset_active_square(self):
        self.active_square = None
