import json

class GameLogger(object):
    def __init__(self, fname, players, indent=None):
        self.indent = indent
        self.match = []
        
        self.set_fname(fname)
        
        for player in players:
            self.match.append("P{} {}".format(player.marker_type, player.name))
    
    def clear(self):
        self.match = []
    
    def set_fname(self, fname):
        self.fname = fname + ".json"
    
    def gen_file(self):
        with open(self.fname, "w") as write_file:
            json.dump(self.match, write_file, indent=self.indent)
    
    def put_marker(self, marker_type, x, y):
        self.match.append("PUT P{}: {} {}".format(marker_type, x, y))
    
    def win(self, marker_type):
        self.match.append("WIN P{}".format(marker_type))
    
    def surrender(self, marker_type):
        self.match.append("SUR P{}".format(marker_type)) 
