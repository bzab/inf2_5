import pyglet
from app import resources

class Sprite(pyglet.sprite.Sprite):
    def __init__(self, *args, **kwargs):
        super(Sprite, self).__init__(*args, **kwargs)

        #print "ADD: ", self
        
    def __repr__(self):
        return "<%s at x=%d, y=%d>" % (self.__class__.__name__, self.x, self.y)
    
    def delete(self):
        super(Sprite, self).delete()
        #print "DEL: ", self

class ClickableSprite(Sprite):
    def __init__(self, app, *args, **kwargs):
        super(ClickableSprite, self).__init__(*args, **kwargs)
        
        self.app=app
        self.app.win.push_handlers(self) 

    def delete(self):
        self.app.win.remove_handlers(self)
        super(ClickableSprite, self).delete()
    
    def is_inside(self, x, y):
        return x > self.x and x < self.x + self.width and \
            y > self.y and y < self.y + self.height

    def on_mouse_press(self, x, y, button, modifiers):
        if self.is_inside(x,y):
            pass
            #print 'mouse pressed on sprite', self

class Marker(Sprite):
    def __init__(self, marker_type, *args, **kwargs):
        if marker_type == 1:
            super(Marker, self).__init__(img=resources.images['o_marker'], 
              *args, **kwargs)  
        elif marker_type == 2:
            super(Marker, self).__init__(img=resources.images['x_marker'], 
              *args, **kwargs) 
        else:
            print 'Wrong marker type!!!', marker_type
            marker_type = -1
            super(Marker, self).__init__(img=resources.images['unknown_marker'],
              *args, **kwargs)
            
        self.type = marker_type
    
class Player(object):
    def __init__(self, marker_type, name):
        self.marker_type = marker_type
        self.name = name

class Action(object):
    def __init__(self, player, x, y):
        self.player = player
        self.x = x
        self.y = y

    def __str__(self):
        return "P{p}: ({x}, {y})".format(p=self.player, x=self.x, y=self.y)

    def __repr__(self):
        return "Action({p}, {x}, {y})".format(p=self.player, x=self.x, y=self.y)

    def __eq__(self, other):
        return (self.__class__ == other.__class__ and self.x == other.x and 
           self.y == other.y and self.player == other.player)

    def __hash__(self):
        return hash((self.x, self.y, self.player))

class Field(object):
    def __init__(self):
        self._marker = None
    
    def __str__(self):
        return str(self._marker)

    def __repr__(self):
        return "Field({})".format(self.__str__())
    
    @property
    def marker(self):
        return self._marker
    
    @marker.setter
    def marker(self, marker_type):
        self._marker = marker_type
        
    @marker.deleter
    def marker(self):
        del self._marker
    
    def is_empty(self):
        return not self._marker

class Square(Sprite):
    def __init__(self, i, j, marker_group, *args, **kwargs):
        super(Square, self).__init__(img=resources.images['square'], 
          x = i*resources.images['square'].width, 
          y = j*resources.images['square'].height, 
          *args, **kwargs)
        
        self.marker = None
        self.marker_group = marker_group
    
    def set_marker(self, marker_type):
        self.marker = Marker(
                        marker_type=marker_type, 
                        x=self.x,
                        y=self.y,
                        batch=self.batch,
                        group=self.marker_group,)
