import pyglet
from app import board, entities, game_logger, hud, resources, utils

CAMERA_SPEED = 5
INIT_SIZE = 15
LIMIT_DIST = 10
MAX_ZOOM = 4

class App(object):

    def __init__(self, window_res_x, window_res_y, fs=False):
        self.win = pyglet.window.Window(
            width=window_res_x, height=window_res_y,
            resizable=True, fullscreen=fs)
        self.players = (
          entities.Player(marker_type=1, name='player 1'), 
          entities.Player(marker_type=2, name='player 2'),
          )
        self.board = board.Board(app=self, init_size=INIT_SIZE)
        self.camera = Camera(width=window_res_x, height=window_res_y)
        self.camera.center_at(
          INIT_SIZE*self.board.board_renderer.square_width/2,
          INIT_SIZE*self.board.board_renderer.square_height/2)
        self.hud = Hud(app=self)
        self.key_handler = pyglet.window.key.KeyStateHandler()
        
        # TODO: Dopracowac event handling
        self.win.push_handlers(self.on_draw, self.on_key_press,
                               self.on_mouse_drag, self.on_mouse_scroll,
                               self.on_resize)
        self.win.push_handlers(self.key_handler)

        self.current_player = self.board.board_state.current_player
        self.hud.set_indicator(self.current_player)

        
    def end_turn(self):
        self.current_player = self.board.board_state.current_player
        self.hud.set_indicator(self.current_player)
    
    def on_draw(self):
        self.win.clear()
        
        # Draw board
        pyglet.gl.glMatrixMode(pyglet.gl.GL_MODELVIEW)
        pyglet.gl.glLoadIdentity()
        
        pyglet.gl.glTranslatef(-self.camera.pos.x, -self.camera.pos.y, 0)

        pyglet.gl.glPushMatrix()
        pyglet.gl.glTranslatef(0,0,0)
        
        pyglet.gl.glScalef(self.camera.zoom, self.camera.zoom,
                           self.camera.zoom)
        pyglet.gl.glTexParameteri(pyglet.gl.GL_TEXTURE_2D, pyglet.gl.GL_TEXTURE_MAG_FILTER, pyglet.gl.GL_NEAREST) 
        pyglet.gl.glTexParameteri(pyglet.gl.GL_TEXTURE_2D, pyglet.gl.GL_TEXTURE_MIN_FILTER, pyglet.gl.GL_NEAREST)
        self.board.draw()
        pyglet.gl.glScalef(1.0, 1.0, 1.0)
        pyglet.gl.glPopMatrix()

        #Draw HUD
        pyglet.gl.glLoadIdentity()
        pyglet.gl.glTexParameteri(pyglet.gl.GL_TEXTURE_2D, pyglet.gl.GL_TEXTURE_MAG_FILTER, pyglet.gl.GL_NEAREST) 
        pyglet.gl.glTexParameteri(pyglet.gl.GL_TEXTURE_2D, pyglet.gl.GL_TEXTURE_MIN_FILTER, pyglet.gl.GL_NEAREST)
        self.hud.draw()
        
        
    def on_key_press(self, symbol, modifiers):
        if(symbol == pyglet.window.key.F10):
            self.win.set_fullscreen(fullscreen = not self.win.fullscreen)
    
    def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
        if buttons & pyglet.window.mouse.RIGHT:
            drag_vec = utils.IntVector2(-dx,-dy)
            self.camera.move(drag_vec)
            return pyglet.event.EVENT_HANDLED
        
    def on_mouse_scroll(self, x, y, scroll_x, scroll_y):
        self.camera.change_zoom(n=scroll_y, x=x, y=y)

    def on_resize(self, width, height): #TODO: Wysrodkuj!
        pyglet.gl.glViewport(0, 0, width, height)
   
        pyglet.gl.glMatrixMode(pyglet.gl.GL_PROJECTION)
        pyglet.gl.glLoadIdentity()

        w = self.win.width // 2
        h = self.win.height // 2

        pyglet.gl.glOrtho(-w, w, -h, h, -1.0, 1.0)  
        
        # Update camera size
        self.camera.update_relative_center(width=width, height=height)
        
        # Update HUD
        self.hud.resize(width=width, height=height)
    
    def reset(self):
        self.board.reset()
        self.camera.reset()
        self.camera.center_at(INIT_SIZE*self.board.board_renderer.square_width/2,
          INIT_SIZE*self.board.board_renderer.square_height/2)
        self.hud.destroy_popup()
    
    def update(self, dt):
        # Move camera
        if (self.key_handler[pyglet.window.key.LEFT] or 
          self.key_handler[pyglet.window.key.RIGHT] or
          self.key_handler[pyglet.window.key.UP] or
          self.key_handler[pyglet.window.key.DOWN]):
            camera_move_vec = utils.IntVector2(0,0)
            if self.key_handler[pyglet.window.key.LEFT]:
                camera_move_vec += (-1,0)
            if self.key_handler[pyglet.window.key.RIGHT]:
                camera_move_vec += (1,0)
            if self.key_handler[pyglet.window.key.UP]:
                camera_move_vec += (0,1)
            if self.key_handler[pyglet.window.key.DOWN]:
                camera_move_vec += (0,-1)
            camera_move_vec *= CAMERA_SPEED
            self.camera.move(camera_move_vec)
        
        self.board.update(dt)                 
        
class Camera(object):
    def __init__(self, x=0, y=0, width=0, height=0):
        self.pos = utils.IntVector2(x,y)
        self.relative_center = utils.IntVector2(width, height)
        self.zoom = 1
    
    def center_at(self, relative_x, relative_y):
        vec = (relative_x, relative_y) - self.relative_center
        self.move(vec)   
    
    def change_zoom(self, n, x, y):
        old_zoom = self.zoom
        z = self.zoom + n
        self.zoom = max(min(z, MAX_ZOOM), 1)
        
        if self.zoom != old_zoom:
            pos_relative_cursor = (x,y) - self.relative_center
            # Center at cursor
            self.pos += pos_relative_cursor
            # Change position to position after zooming
            self.pos = (((self.pos+self.relative_center) * self.zoom)
                        / old_zoom) - self.relative_center
            # Keep relative cursor position same as before zooming
            self.pos -= pos_relative_cursor
            
                   
    def move(self, vec):
        self.pos += vec
        #print 'Cam new pos', self.pos
    
    def update_relative_center(self, width, height):
        diag = utils.IntVector2(width, height) / 2
        self.move(self.relative_center - diag)
        self.relative_center = diag
        #print 'Relative center',self.relative_center
    
    def reset(self):
        self.pos = utils.IntVector2(0, 0)
        self.zoom = 1


class Hud(object):
    def __init__(self, app):
        self.app = app
        self.batch = pyglet.graphics.Batch()
        
        self.height = app.win.height
        self.width = app.win.width
        
        self.pgOG_bg = pyglet.graphics.OrderedGroup(0)
        self.pgOG_mg = pyglet.graphics.OrderedGroup(1)
        self.pgOG_fg = pyglet.graphics.OrderedGroup(2)
        
        self.players_box = hud.PlayersBox(players=self.app.players,
          x=50, y=self.height-152, batch=self.batch,
          group_bg=self.pgOG_bg, group_mg=self.pgOG_mg, group_fg=self.pgOG_fg)
    
        self.popup = None
    
    def create_popup(self, text=""):
        self.popup = hud.TextBox(text=text, x=self.width//2,
          y=self.height//2, batch=self.batch, group_bg=self.pgOG_bg, 
          group_fg=self.pgOG_fg, center=True)
        self.popup.move(-self.popup.width//2, -self.popup.height//2)
    
    def destroy_popup(self):
        self.popup = None      
        
    def draw(self):
        self.batch.draw()
    
    def set_indicator(self, player):
        self.players_box.set_indicator(player)
        
    def resize(self, width, height): 
        self.players_box.set_position(x=self.players_box.x, y=height-152)
        if self.popup:
            self.popup.set_position(x=(width-self.popup.width)//2,
              y=(height-self.popup.height)//2)
        self.width = width
        self.height = height

app = App(window_res_x=1280, window_res_y=720, fs=False)  



pyglet.clock.schedule_interval(app.update, 1/120.0)

pyglet.app.run()




